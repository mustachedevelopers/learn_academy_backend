package article

type Repository interface {
	Create(*Article) (*Article, error)
	FindById(int64) (*Article, error)
	Delete(int64) error
	Update(int64, *Article) (*Article, error)
}

type Service interface {
	Create(*Article) (*Article, error)
	FindById(int64) (*Article, error)
	Delete(int64) error
	Update(int64, *Article) (*Article, error)
}

type articleService struct {
	repository Repository
}

func NewService(repository Repository) Service {
	return &articleService{repository: repository}
}

func (as *articleService) Create(article *Article) (*Article, error) {
	if err := article.IsValid(); err != nil {
		return nil, err
	}

	return as.repository.Create(article)

}

func (as *articleService) FindById(id int64) (*Article, error) {
	return as.repository.FindById(id)
}

func (as *articleService) Delete(id int64) error {
	return as.repository.Delete(id)
}

func (as *articleService) Update(id int64, article *Article) (*Article, error) {
	return as.repository.Update(id, article)
}