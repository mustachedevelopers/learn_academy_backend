package article

import (
	"errors"
	"time"
)

type ArticleUser struct {
	Id int64 `json:"id"`
	FirstName string `json:"firstName"`
	LastName string `json:"lastName"`
	Avatar string `json:"avatar"`
}

type ArticleComment struct {
	Id int64 `json:"id"`
	User ArticleUser `json:"user"`
	Body string `json:"body"`
	CreatedAt time.Time `json:"createdAt"`
}

type Article struct {
	Id int64 `json:"id"`
	User ArticleUser `json:"user"`
	Comments []ArticleComment `json:"comments"`
	Title string `json:"title"`
	Description string `json:"description"`
	Published bool `json:"published"`
	Thumbnail string `json:"thumbnail"`
	Duration string `json:"duration"`
}


func (article *Article) IsValid() error {
	if article.Title == "" || article.Description == "" {
		return errors.New("article can not without title and description")
	}

	return nil
}

