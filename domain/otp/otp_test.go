package otp_test

import (
	"testing"
	"time"

	"gitlab.com/MustachDevelopers/learn_academy_backend/domain/otp"
)

type isExpiredTestCaseStruct struct {
	name     string
	input    *otp.Otp
	expected bool
}

var isExpiredCases = []isExpiredTestCaseStruct{
	{name: "valid-otp",
		input: &otp.Otp{Code: 1111, Expired: time.Now().UTC().Add(otp.ExpirationTime * time.Minute).Unix(),
			Attemp: otp.AttempTime},
		expected: false,
	},
	{
		name:     "time-expired-otp",
		input:    &otp.Otp{Code: 1111, Expired: time.Now().UTC().Add(-time.Minute).Unix(), Attemp: otp.AttempTime},
		expected: true,
	},
	{
		name: "attemp-expired-otp",
		input: &otp.Otp{Code: 1111, Expired: time.Now().UTC().Add(otp.ExpirationTime * time.Minute).Unix(),
			Attemp: 0},
		expected: true,
	},
	{
		name:     "attemp-and-time-expired-otp",
		input:    &otp.Otp{Code: 1111, Expired: time.Now().UTC().Add(-time.Minute).Unix(), Attemp: 0},
		expected: true,
	},
}

func Test_IsExpired(t *testing.T) {
	for _, tc := range isExpiredCases {
		if out := tc.input.IsExpired(); out != tc.expected {
			t.Errorf("fail in test %s i expected: %v but got %v\n", tc.name, tc.expected, out)
		}
	}
}

func Test_NewOtp(t *testing.T) {
	res := otp.NewOtp()

	if res == nil {
		t.Error("fail in test NewOtp i expected otp but got nil")
	}

	if res.IsExpired() {
		t.Error("fail in test NewOtp i expected valid otp but got expired otp")
	}

}
