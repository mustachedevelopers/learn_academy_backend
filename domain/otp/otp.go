package otp

import (
	"gitlab.com/MustachDevelopers/learn_academy_backend/utils/rand_utils"
	"time"
)

const (
	ExpirationTime = 15
	AttempTime     = 5
)

type Otp struct {
	Code     int    `json:"code"`
	Expired  int64  `json:"expired"`
	Attemp   int    `json:"attemp"`
	Phone    string `json:"phone"`
	ClientId int64  `json:"clientId"`
}

func NewOtp() *Otp {
	return &Otp{
		Code:    rand_utils.GetRandomDigit(1000, 9999),
		Expired: time.Now().UTC().Add(ExpirationTime * time.Minute).Unix(),
		Attemp:  AttempTime,
	}
}

func (otp *Otp) IsExpired() bool {
	// if otp attemped
	if otp.Attemp <= 0 {
		return true
	}
	// if otp expire time is less than now return true
	return time.Unix(otp.Expired, 0).Before(time.Now().UTC())
}
