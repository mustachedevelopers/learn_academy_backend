package otp

import (
	"strconv"

	"gitlab.com/MustachDevelopers/learn_academy_backend/utils/errors"
)

type Repository interface {
	Save(*Otp) *errors.RestErr
	FindOne(int, string) (*Otp, *errors.RestErr)
}

type Notification interface {
	SendSMS(string, string) *errors.RestErr
}

type Service interface {
	Request(string, int64) (*Otp, *errors.RestErr)
	Validate(int, int64, string) (*Otp, *errors.RestErr)
}

type otpService struct {
	repository   Repository
	notification Notification
}

func NewService(repository Repository, notification Notification) Service {
	return &otpService{
		repository:   repository,
		notification: notification,
	}
}

func (ots *otpService) Request(phone string, cid int64) (*Otp, *errors.RestErr) {

	otp := NewOtp()
	otp.Phone = phone
	otp.ClientId = cid

	err := ots.repository.Save(otp)

	if err != nil {
		return nil, err
	}

	err = ots.notification.SendSMS(otp.Phone, strconv.Itoa(otp.Code))

	if err != nil {
		return nil, err
	}

	return otp, nil

}

func (ots *otpService) Validate(code int, cid int64, phone string) (*Otp, *errors.RestErr) {

	otp, err := ots.repository.FindOne(code, phone)

	if err != nil {
		return nil, err
	}

	if otp.ClientId != cid {
		return nil, errors.NewBadRequest("client id and otp is diffrent")
	}

	if otp.IsExpired() {
		return nil, errors.NewBadRequest("otp was expired")
	}

	return otp, nil
}
