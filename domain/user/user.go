package user

import "encoding/json"

type User struct {
	Id        int64  `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	Password  string `json:"password"`
	Role      string `json:"role"`
}


func (u *User) Marshall() string {
	user, _ := json.Marshal(u)
	return string(user)
}

