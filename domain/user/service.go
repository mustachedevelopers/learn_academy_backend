package user

import "gitlab.com/MustachDevelopers/learn_academy_backend/utils/errors"

type Repository interface {
	FindOrCreate(*User) (*User, *errors.RestErr)
	FindByPhone(string) (*User, *errors.RestErr)
	Create(*User) (*User, *errors.RestErr)
	FindById(int64) (*User, *errors.RestErr)
	Find(string) ([]*User)
	Update(int64, *User) (*User, *errors.RestErr)
	Delete(int64) *errors.RestErr
}

type Service interface {
	LoginWithOtp(string) (*User, *errors.RestErr)
	FindByPhone(string) (*User, *errors.RestErr)
	Create(*User) (*User, *errors.RestErr)
	FindById(int64) (*User, *errors.RestErr)
	Find(string) ([]*User)
	Update(int64, *User) (*User, *errors.RestErr)
	Delete(int64) *errors.RestErr
}

type userService struct {
	repository Repository
}

func NewService(repository Repository) Service {
	return &userService{
		repository: repository,
	}
}

func (us *userService) LoginWithOtp(phone string) (*User, *errors.RestErr) {
	user := new(User)

	user.Phone = phone

	return us.repository.FindOrCreate(user)
}

func (us *userService) FindByPhone(phone string) (*User, *errors.RestErr) {
	return us.repository.FindByPhone(phone)
}

func (us *userService) Create(user *User) (*User, *errors.RestErr) {
	return us.repository.Create(user)
}

func (us *userService) FindById(id int64) (*User, *errors.RestErr) {
	return us.repository.FindById(id)
}

func (us *userService) Find(term string) ([]*User) {
	return us.repository.Find(term)
}

func (us *userService) Update(id int64, user *User) (*User, *errors.RestErr) {
	return us.repository.Update(id, user)
}

func (us *userService) Delete(id int64) *errors.RestErr {
	return us.repository.Delete(id)
}