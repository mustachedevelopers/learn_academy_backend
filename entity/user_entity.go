package entity

import (
	"time"
)


type User struct {
	ID        int64  `gorm:"primaryKey;autoIncrement" json:"id"`
	FirstName string `gorm:"<-;type:varchar(255)" json:"firstName"`
	LastName  string `gorm:"<-;type:varchar(255)" json:"lastName"`
	Email     string `gorm:"<-;type:varchar(255);unique" json:"email"`
	Phone     string `gorm:"<-;type:varchar(11);not null;unique" json:"phone"`
	Password  string `gorm:"<-;type:varchar(255)" json:"password"`
	Role      string `gorm:"<-;type:varchar(30);default:user" json:"role"`
	CreatedAt time.Time `gorm:"<-;type:timestamp;not null" json:"-"`
	UpdatedAt time.Time `gorm:"<-;type:timestamp" json:"-"`
}

