package entity

import "time"

type Otp struct {
	ID        uint64    `gorm:"primaryKey;autoIncrement" json:"-"`
	Code      int       `gorm:"<-;type:bigint;not null;unique" json:"code"`
	Expired   int64     `gorm:"<-;type:bigint;not null" json:"expired"`
	Phone     string    `gorm:"<-;type:varchar(11);not null" json:"phone"`
	ClientId  int64     `gorm:"<-;type:bigint;not null;default:0" json:"clientId"`
	Attemp    int       `gorm:"<-;type:int;default:5" json:"attemp"`
	CreatedAt time.Time `gorm:"<-;type:timestamp;not null" json:"-"`
	UpdatedAt time.Time `gorm:"<-;type:timestamp" json:"-"`
}
