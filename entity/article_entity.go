package entity

type Article struct {
	ID int64 `gorm:"primaryKey;autoIncerement;" json:"id"`
	Title string `gorm:"<-;type:varchar(255);not null;" json:"title"`
	Description string `gorm:"<-;type:text" json:"description"`
}