package notification

import (
	"fmt"

	"github.com/kavenegar/kavenegar-go"
	"gitlab.com/MustachDevelopers/learn_academy_backend/utils/errors"
)

type NotificationService interface {
	SendSMS(string, string) *errors.RestErr
}

type notificationService struct {
	smsService *kavenegar.Kavenegar
}

func NewNotificationService(api *kavenegar.Kavenegar) NotificationService {
	return &notificationService{
		smsService: api,
	}
}

func (ns *notificationService) SendSMS(phone string, code string) *errors.RestErr {
	res, err := ns.smsService.Verify.Lookup(phone, "registerverify", code, nil)

	if err != nil {
		return errors.NewInternalServerError("SMS Provider send error")
	}
	fmt.Println(res)
	return nil
}
