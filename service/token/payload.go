package token

import (
	"encoding/json"
	"errors"
	"time"

	"github.com/google/uuid"
)

var (
	ErrExpireToken  = errors.New("token is expired")
	ErrInvalidToken = errors.New("token is invalid")
)

type Payload struct {
	ID        uuid.UUID `json:"id"`
	Phone     string    `json:"phone"`
	IssuedAt  time.Time `json:"issued_at"`
	ExpiredAt time.Time `json:"expired_at"`
}

func NewPayload(phone string, duration time.Duration) (*Payload, error) {
	tokenId, err := uuid.NewUUID()

	if err != nil {
		return nil, errors.New("invalid payload")
	}

	payload := &Payload{
		ID:        tokenId,
		Phone:     phone,
		IssuedAt:  time.Now(),
		ExpiredAt: time.Now().Add(duration),
	}

	return payload, nil
}

func (payload *Payload) Valid() error {

	if time.Now().After(payload.ExpiredAt) {
		return ErrExpireToken
	}

	return nil
}

func (payload *Payload) Marshall() string {
	j, _ := json.Marshal(payload)

	return string(j)
}
