package token

import (
	"fmt"
	"time"

	"github.com/o1egl/paseto"
	"golang.org/x/crypto/chacha20poly1305"
)

type PasetoMaker struct {
	paseto        *paseto.V2
	symmetricKey []byte
}

func NewPaseto(symmeryticKey string) (Maker, error) {
	if len(symmeryticKey) < chacha20poly1305.KeySize {
		return nil, fmt.Errorf("invalid key size: length of symmerytic key is less than %d", chacha20poly1305.KeySize)
	}

	return &PasetoMaker{
		paseto:        paseto.NewV2(),
		symmetricKey: []byte(symmeryticKey),
	}, nil
}

func (maker *PasetoMaker) CreateToken(phone string, duration time.Duration) (string, error) {
	payload, err := NewPayload(phone, duration)

	if err != nil {
		return "", err
	}

	return maker.paseto.Encrypt(maker.symmetricKey, payload, nil)
}

func (maker *PasetoMaker) VerifyToken(token string) (*Payload, error) {
	payload := &Payload{}

	err := maker.paseto.Decrypt(token, maker.symmetricKey, payload, nil)

	if err != nil {
		return nil, err
	}

	err = payload.Valid()

	if err != nil {
		return nil, err
	}

	return payload, nil
}
