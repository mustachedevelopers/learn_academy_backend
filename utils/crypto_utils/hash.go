package crypto_utils

import (
	"gitlab.com/MustachDevelopers/learn_academy_backend/utils/errors"
	"golang.org/x/crypto/bcrypt"
)

func Hash(value string) (string, *errors.RestErr) {
	hash, err := bcrypt.GenerateFromPassword([]byte(value), bcrypt.DefaultCost)

	if err != nil {
		return "", errors.NewInternalServerError("internal server error in hash password")
	}

	return string(hash), nil
}

func Compare(hash string, value string) *errors.RestErr {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(value))

	if err != nil {
		return errors.NewBadRequest("password is incorrect")
	}

	return nil
}
