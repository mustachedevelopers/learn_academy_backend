package rand_utils

import (
	"math/rand"
	"time"
)

func GetRandomDigit(min int, max int) int {

	rand.Seed(time.Now().UnixNano())

	return rand.Intn(max-min) + min
}
