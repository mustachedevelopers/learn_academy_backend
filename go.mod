module gitlab.com/MustachDevelopers/learn_academy_backend

go 1.16

require (
	github.com/aead/chacha20poly1305 v0.0.0-20201124145622-1a5aba2a8b29 // indirect
	github.com/go-redis/redis/v8 v8.11.4 // indirect
	github.com/gofiber/fiber/v2 v2.17.0 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/google/uuid v1.3.0
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/kavenegar/kavenegar-go v0.0.0-20200629080648-6e28263b7162 // indirect
	github.com/o1egl/paseto v1.0.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5 // indirect
	golang.org/x/sys v0.0.0-20210820121016-41cdb8703e55 // indirect
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.13 // indirect
)
