package routers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/MustachDevelopers/learn_academy_backend/http/handlers/ping"
)



func PingRoute(prefix string, router *fiber.App) {
	// set group prefix route
	r := router.Group(prefix)

	// get instanse of ping handler
	pHandler := ping.NewHandler()

	// routing
	r.Get("", pHandler.GetPing)
}