package routers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/MustachDevelopers/learn_academy_backend/domain/article"
	"gitlab.com/MustachDevelopers/learn_academy_backend/http/handlers/articles"
	"gitlab.com/MustachDevelopers/learn_academy_backend/http/middlewares/auth_middleware"
	article_repository "gitlab.com/MustachDevelopers/learn_academy_backend/repository/article_repositroy"
)

func NewArticle(prefix string, router *fiber.App) {
	r := router.Group(prefix, auth_middleware.Authorization, auth_middleware.AdminMiddleware)

	articleRepository := article_repository.NewRepository()
	articleService := article.NewService(articleRepository)
	articleHandler := articles.NewHandler(articleService)

	r.Get("/:article_id", articleHandler.FindById)
	r.Post("", articleHandler.Create)
	r.Put("/:article_id", articleHandler.Update)
	r.Patch("/:article_id/published", articleHandler.Update)
	r.Delete("/:article_id", articleHandler.Delete)
}
