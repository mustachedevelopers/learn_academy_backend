package routers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/MustachDevelopers/learn_academy_backend/domain/user"
	"gitlab.com/MustachDevelopers/learn_academy_backend/http/handlers/users"
	"gitlab.com/MustachDevelopers/learn_academy_backend/http/middlewares/auth_middleware"
	"gitlab.com/MustachDevelopers/learn_academy_backend/infrastructure/db"
	"gitlab.com/MustachDevelopers/learn_academy_backend/repository/user_repository"
)

func NewUserRoute(prefix string, router *fiber.App) {
	r := router.Group(prefix, auth_middleware.Authorization, auth_middleware.AdminMiddleware)

	userRepository := user_repository.NewRepository(db.Client)
	userService := user.NewService(userRepository)
	userHandler := users.NewHandler(userService)

	r.Post("", userHandler.Create)
	r.Get("", userHandler.Find)
	r.Get("/:user_id", userHandler.FindOne)
	r.Put("/:user_id", userHandler.Update)
	r.Delete("/:user_id", userHandler.Delete)
}
