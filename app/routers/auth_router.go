package routers

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/MustachDevelopers/learn_academy_backend/config"
	"gitlab.com/MustachDevelopers/learn_academy_backend/domain/otp"
	"gitlab.com/MustachDevelopers/learn_academy_backend/domain/user"
	"gitlab.com/MustachDevelopers/learn_academy_backend/http/handlers/auth"
	"gitlab.com/MustachDevelopers/learn_academy_backend/http/middlewares/auth_middleware"
	"gitlab.com/MustachDevelopers/learn_academy_backend/infrastructure/db"
	"gitlab.com/MustachDevelopers/learn_academy_backend/infrastructure/sms"
	"gitlab.com/MustachDevelopers/learn_academy_backend/repository/otp_repository"
	"gitlab.com/MustachDevelopers/learn_academy_backend/repository/user_repository"
	"gitlab.com/MustachDevelopers/learn_academy_backend/service/notification"
	"gitlab.com/MustachDevelopers/learn_academy_backend/service/token"
)

func NewAuth(prefix string, router *fiber.App) {
	r := router.Group(prefix)

	otpRepository := otp_repository.NewRepository(db.Client)
	userRepository := user_repository.NewRepository(db.Client)
	notificationService := notification.NewNotificationService(sms.ConnectSMS())
	otpService := otp.NewService(otpRepository, notificationService)
	userService := user.NewService(userRepository)

	tokenMaker, err := token.NewPaseto(config.CommonConfig.Symmetric)

	if err != nil {
		fmt.Println(err.Error())
	}
	authHandler := auth.NewHandler(otpService, userService, tokenMaker)

	r.Post("/otp/request", authHandler.RequestOtp)
	r.Post("/otp/login", authHandler.LoginWithOtp)
	r.Get("/otp", auth_middleware.Authorization, func(c *fiber.Ctx) error {
		return c.JSON(map[string]string{"payload": string(c.Request().Header.Peek("user"))})
	})
}
