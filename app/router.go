package app

import "gitlab.com/MustachDevelopers/learn_academy_backend/app/routers"

func url_mapping() {

	// set routers
	routers.PingRoute("ping", Router)

	routers.NewAuth("auth", Router)
	routers.NewUserRoute("admin/users", Router)
}
