package app

import (
	"fmt"
	"os"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/MustachDevelopers/learn_academy_backend/config"
	"gitlab.com/MustachDevelopers/learn_academy_backend/infrastructure/db"
	"gitlab.com/MustachDevelopers/learn_academy_backend/infrastructure/redis"
)

func init() {
	var env string

	if len(os.Args) > 1 {
		env = os.Args[1]
	}

	config.ParseEnv(env)

	// set some infrastructure
	db.SetDB()
	redis.ConnectRedis()
}

var (
	Router *fiber.App
)

func StartApplication() {
	Router = fiber.New()

	// routing urls
	url_mapping()

	Router.Listen(fmt.Sprintf(":%s", config.CommonConfig.Port))
}
