package auth_middleware

import (
	"encoding/json"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/MustachDevelopers/learn_academy_backend/domain/user"
	"gitlab.com/MustachDevelopers/learn_academy_backend/utils/errors"
)

func AdminMiddleware(ctx *fiber.Ctx) error {
	var u user.User

	authUser := ctx.Request().Header.Peek("user")

	err := json.Unmarshal(authUser, &u)

	if err != nil {
		rerr := errors.NewUnAuthenticated("unknown request header")
		return ctx.Status(rerr.Status).JSON(rerr)
	}

	if u.Role == "admin" {
		return ctx.Next()
	}

	rerr := errors.NewUnAuthenticated("forbidden resource")
	return ctx.Status(rerr.Status).JSON(rerr)
}
