package auth_middleware

import (
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/MustachDevelopers/learn_academy_backend/config"
	"gitlab.com/MustachDevelopers/learn_academy_backend/domain/user"
	"gitlab.com/MustachDevelopers/learn_academy_backend/infrastructure/db"
	"gitlab.com/MustachDevelopers/learn_academy_backend/repository/user_repository"
	"gitlab.com/MustachDevelopers/learn_academy_backend/service/token"
	"gitlab.com/MustachDevelopers/learn_academy_backend/utils/errors"
)

func Authorization(ctx *fiber.Ctx) error {
	authorization := string(ctx.Request().Header.Peek("Authorization"))

	if authorization == "" {
		err := errors.NewUnAuthenticated("you are not authenticated")
		return ctx.Status(err.Status).JSON(err)
	}

	s := strings.Fields(authorization)
	authType, accessToken := s[0], s[1]

	if authType != "Bearer" {
		err := errors.NewUnAuthenticated("invalid token type")
		return ctx.Status(err.Status).JSON(err)
	}

	tokenMaker, err := token.NewPaseto(config.CommonConfig.Symmetric)

	if err != nil {
		err := errors.NewUnAuthenticated("invalid token")
		return ctx.Status(err.Status).JSON(err)
	}

	payload, err := tokenMaker.VerifyToken(accessToken)

	if err != nil {
		cerr := errors.NewUnAuthenticated(err.Error())
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	userRepo := user_repository.NewRepository(db.Client)
	userService := user.NewService(userRepo)

	u, uErr := userService.FindByPhone(payload.Phone)

	if uErr != nil {
		cerr := errors.NewUnAuthenticated(uErr.Message)
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	// TODO: Find Better Way To Bind Payload
	ctx.Request().Header.Add("user", u.Marshall())

	return ctx.Next()
}
