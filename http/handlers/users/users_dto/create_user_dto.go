package users_dto

import (
	"encoding/json"
	"strings"

	"gitlab.com/MustachDevelopers/learn_academy_backend/domain/user"
	"gitlab.com/MustachDevelopers/learn_academy_backend/utils/errors"
)

type CreateUserDto struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	Password  string `json:"password"`
	Role      string `json:"role"`
}

func (dto *CreateUserDto) Validate() *errors.RestErr {
	dto.Email = strings.ToLower(strings.TrimSpace(dto.Email))

	if dto.Email == "" {
		return errors.NewBadRequest("email is required")
	}

	if len(dto.Phone) != 11 {
		return errors.NewBadRequest("invalid phone number")
	}

	return nil
}

func (dto *CreateUserDto) ToDomain() (*user.User, *errors.RestErr) {
	ud := new(user.User)

	bytes, err := json.Marshal(dto)

	if err != nil {
		return nil, errors.NewBadRequest("invalid json body")
	}

	if err := json.Unmarshal(bytes, ud); err != nil {
		return nil, errors.NewBadRequest("invalid json body")
	}

	return ud, nil

}
