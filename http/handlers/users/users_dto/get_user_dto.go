package users_dto

import (
	"encoding/json"

	"gitlab.com/MustachDevelopers/learn_academy_backend/domain/user"
)


type GetUserDto struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	Role      string `json:"role"`
}

func (dto *GetUserDto) Marshall(u *user.User) {
	bytes, _ := json.Marshal(u)
	_ = json.Unmarshal(bytes, dto)
}