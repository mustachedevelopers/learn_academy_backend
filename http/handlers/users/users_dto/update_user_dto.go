package users_dto

import (
	"encoding/json"

	"gitlab.com/MustachDevelopers/learn_academy_backend/domain/user"
	"gitlab.com/MustachDevelopers/learn_academy_backend/utils/errors"
)

type UpdateUserDto struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	Role      string `json:"role"`
}

func (dto *UpdateUserDto) ToDomain() (*user.User, *errors.RestErr) {
	ud := new(user.User)

	bytes, err := json.Marshal(dto)

	if err != nil {
		return nil, errors.NewBadRequest("invalid json body")
	}

	if err := json.Unmarshal(bytes, ud); err != nil {
		return nil, errors.NewBadRequest("invalid json body")
	}

	return ud, nil

}
