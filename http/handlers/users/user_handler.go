package users

import (
	"net/http"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/MustachDevelopers/learn_academy_backend/domain/user"
	"gitlab.com/MustachDevelopers/learn_academy_backend/http/handlers/users/users_dto"
	"gitlab.com/MustachDevelopers/learn_academy_backend/utils/errors"
)

type userHandler struct {
	userService user.Service
}

func NewHandler(userService user.Service) *userHandler {
	return &userHandler{
		userService: userService,
	}
}

func (uh *userHandler) Create(ctx *fiber.Ctx) error {
	userDto := new(users_dto.CreateUserDto)

	if err := ctx.BodyParser(userDto); err != nil {
		cerr := errors.NewBadRequest("invalid json body")
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	if err := userDto.Validate(); err != nil {
		return ctx.Status(err.Status).JSON(err)
	}

	ud, err := userDto.ToDomain()

	if err != nil {
		return ctx.Status(err.Status).JSON(err)
	}

	createdUser, err := uh.userService.Create(ud)

	if err != nil {
		return ctx.Status(err.Status).JSON(err)
	}

	getUserDto := new(users_dto.GetUserDto)
	getUserDto.Marshall(createdUser)
	return ctx.Status(http.StatusCreated).JSON(getUserDto)
}

func (uh *userHandler) FindOne(ctx *fiber.Ctx) error {
	userId, err := strconv.Atoi(ctx.Params("user_id"))

	if err != nil {
		cerr := errors.NewBadRequest("invalid user_id params")
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	u, ferr := uh.userService.FindById(int64(userId))

	if ferr != nil {
		return ctx.Status(ferr.Status).JSON(ferr)
	}

	getUserDto := new(users_dto.GetUserDto)
	getUserDto.Marshall(u)

	return ctx.Status(http.StatusOK).JSON(getUserDto)
}

func (uh *userHandler) Find(ctx *fiber.Ctx) error {
	term := ctx.Query("term")

	us := uh.userService.Find(term)

	getUsers := make([]*users_dto.GetUserDto, 0)

	for _, u := range us {
		dto := new(users_dto.GetUserDto)
		dto.Marshall(u)
		getUsers = append(getUsers, dto)
	}

	return ctx.Status(http.StatusOK).JSON(getUsers)
}

func (uh *userHandler) Update(ctx *fiber.Ctx) error {

	userId, err := strconv.Atoi(ctx.Params("user_id"))

	if err != nil {
		cerr := errors.NewBadRequest("invalid user_id params")
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	updateDto := new(users_dto.UpdateUserDto)

	if err := ctx.BodyParser(updateDto); err != nil {
		cerr := errors.NewBadRequest("invalid json body")
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	ud, cerr := updateDto.ToDomain()

	if cerr != nil {
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	updatedUser, cerr := uh.userService.Update(int64(userId), ud)

	if cerr != nil {
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	getUserDto := new(users_dto.GetUserDto)
	getUserDto.Marshall(updatedUser)
	return ctx.Status(http.StatusOK).JSON(getUserDto)
}

func (uh *userHandler) Delete(ctx *fiber.Ctx) error {
	userId, err := strconv.Atoi(ctx.Params("user_id"))
	if err != nil {
		cerr := errors.NewBadRequest("invalid user_id params")
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	if cerr := uh.userService.Delete(int64(userId)); cerr != nil {
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	return ctx.Status(http.StatusOK).JSON(map[string]interface{}{"message": "deleted"})
}
