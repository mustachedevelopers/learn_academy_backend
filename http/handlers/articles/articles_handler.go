package articles

import (
	"net/http"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/MustachDevelopers/learn_academy_backend/domain/article"
	"gitlab.com/MustachDevelopers/learn_academy_backend/http/handlers/articles/articles_dto"
	"gitlab.com/MustachDevelopers/learn_academy_backend/utils/errors"
)

type articleHandler struct {
	articleService article.Service
}

func NewHandler(service article.Service) *articleHandler {
	return &articleHandler{
		articleService: service,
	}
}

func (ah *articleHandler) Create(ctx *fiber.Ctx) error {
	createArticleDto := new(articles_dto.CreateArticleDto)

	if err := ctx.BodyParser(createArticleDto); err != nil {
		cerr := errors.NewBadRequest("invalid json body")
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	if err := createArticleDto.Validate(); err != nil {
		return ctx.Status(err.Status).JSON(err)
	}

	ar := &article.Article{
		Title:       createArticleDto.Title,
		Description: createArticleDto.Description,
		Thumbnail:   createArticleDto.Thumbnail,
		Duration:    createArticleDto.Duration,
	}

	newAr, err := ah.articleService.Create(ar)

	if err != nil {
		cerr := errors.NewBadRequest(err.Error())
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	return ctx.Status(http.StatusOK).JSON(newAr)
}

func (ah *articleHandler) FindById(ctx *fiber.Ctx) error {
	articleId, err := strconv.ParseInt(ctx.Params("article_id"), 10, 64)

	if err != nil || articleId < 1 {
		cerr := errors.NewBadRequest("invalid article_id params")
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	ar, err := ah.articleService.FindById(articleId)

	if err != nil {
		cerr := errors.NewNotFound(err.Error())
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	return ctx.Status(http.StatusOK).JSON(ar)
}

func (ah *articleHandler) Update(ctx *fiber.Ctx) error {
	articleId, err := strconv.ParseInt(ctx.Params("article_id"), 10, 64)

	if err != nil || articleId < 1 {
		cerr := errors.NewBadRequest("invalid article_id params")
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	updateArticleDto := new(articles_dto.UpdateArticleDto)

	if err := ctx.BodyParser(updateArticleDto); err != nil {
		cerr := errors.NewBadRequest("invalid json body")
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	uarticle := &article.Article{
		Title:       updateArticleDto.Title,
		Description: updateArticleDto.Description,
		Thumbnail:   updateArticleDto.Thumbnail,
		Published:   updateArticleDto.Published,
		Duration:    updateArticleDto.Duration,
	}

	ar, err := ah.articleService.Update(articleId, uarticle)

	if err != nil {
		cerr := errors.NewNotFound(err.Error())
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	return ctx.Status(http.StatusOK).JSON(ar)
}


func (ah *articleHandler) Delete(ctx *fiber.Ctx) error {
	articleId, err := strconv.ParseInt(ctx.Params("article_id"), 10, 64)

	if err != nil || articleId < 1{
		cerr := errors.NewBadRequest("invalid article_id params")
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	err = ah.articleService.Delete(articleId)

	if err != nil {
		cerr := errors.NewNotFound("article not found")
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	return ctx.Status(http.StatusNoContent).JSON(map[string]string{"status": "deleted"})
}