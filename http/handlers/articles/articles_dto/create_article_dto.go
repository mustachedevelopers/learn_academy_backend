package articles_dto

import "gitlab.com/MustachDevelopers/learn_academy_backend/utils/errors"

type CreateArticleDto struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	Thumbnail   string `json:"thumbnail"`
	Duration    string `json:"duration"`
}

func (article *CreateArticleDto) Validate() *errors.RestErr {

	if article.Title == "" {
		return errors.NewBadRequest("title field is required")
	}

	if article.Description == "" {
		return errors.NewBadRequest("description field is required")
	}

	if article.Thumbnail == "" {
		return errors.NewBadRequest("thumbnail field is required")
	}

	return nil
}
