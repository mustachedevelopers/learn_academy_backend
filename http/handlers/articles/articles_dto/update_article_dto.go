package articles_dto

type UpdateArticleDto struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	Thumbnail   string `json:"thumbnail"`
	Duration    string `json:"duration"`
	Published   bool   `json:"published"`
}
