package auth

import "gitlab.com/MustachDevelopers/learn_academy_backend/domain/otp"

type GetOtpDto struct {
	Phone   string `json:"phone"`
	Expired int64  `json:"expired"`
	Attemp  int    `json:"attemp"`
}

func (dto *GetOtpDto) Marshall(otp *otp.Otp) {
	dto.Phone = otp.Phone
	dto.Expired = otp.Expired
	dto.Attemp = otp.Attemp
}
