package auth

import "gitlab.com/MustachDevelopers/learn_academy_backend/utils/errors"

type RequestOtpDto struct {
	Phone    string `json:"phone"`
	ClientId int64  `json:"clientId"`
}


func (request *RequestOtpDto) Validate() *errors.RestErr {

	if len(request.Phone) != 11  {
		return errors.NewBadRequest("invalid phone number")
	}

	return nil
}