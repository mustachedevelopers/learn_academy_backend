package auth

import "gitlab.com/MustachDevelopers/learn_academy_backend/utils/errors"

type LoginOtpDto struct {
	Phone    string `json:"phone"`
	Code     int    `json:"code"`
	ClientId int64  `json:"clientId"`
}


func (dto *LoginOtpDto) Validate() *errors.RestErr {

	if dto.ClientId < 0 {
		return errors.NewBadRequest("invalid clientId")
	}

	if len(dto.Phone) != 11 {
		return errors.NewBadRequest("invalid phone number")
	}

	if dto.Code == 0 {
		return errors.NewBadRequest("invalid code")
	}
	

	return nil
}