package auth

import (
	"net/http"


	"github.com/gofiber/fiber/v2"
	"gitlab.com/MustachDevelopers/learn_academy_backend/config"
	"gitlab.com/MustachDevelopers/learn_academy_backend/domain/otp"
	"gitlab.com/MustachDevelopers/learn_academy_backend/domain/user"
	"gitlab.com/MustachDevelopers/learn_academy_backend/service/token"
	"gitlab.com/MustachDevelopers/learn_academy_backend/utils/errors"
)

type LoginHandler interface {
	RequestOtp(*fiber.Ctx) error
	LoginWithOtp(*fiber.Ctx) error
}

type loginHandler struct {
	otpService  otp.Service
	userService user.Service
	tokenMaker  token.Maker
}

func NewHandler(otpService otp.Service, userService user.Service, tokenMaker token.Maker) LoginHandler {
	return &loginHandler{
		otpService:  otpService,
		userService: userService,
		tokenMaker:  tokenMaker,
	}
}

func (h *loginHandler) RequestOtp(ctx *fiber.Ctx) error {
	request := new(RequestOtpDto)

	if err := ctx.BodyParser(request); err != nil {
		cerr := errors.NewBadRequest("invalid json body")
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	if request.Validate() != nil {
		var err = request.Validate()
		return ctx.Status(err.Status).JSON(err)
	}

	otp, err := h.otpService.Request(request.Phone, request.ClientId)

	if err != nil {
		return ctx.Status(err.Status).JSON(err)
	}

	var otpDto GetOtpDto
	otpDto.Marshall(otp)

	return ctx.Status(http.StatusOK).JSON(&otpDto)
}

func (h *loginHandler) LoginWithOtp(ctx *fiber.Ctx) error {
	loginDto := new(LoginOtpDto)

	if err := ctx.BodyParser(loginDto); err != nil {
		cerr := errors.NewBadRequest("invalid json body")
		return ctx.Status(cerr.Status).JSON(cerr)
	}

	if loginDto.Validate() != nil {
		var err = loginDto.Validate()
		return ctx.Status(err.Status).JSON(err)
	}

	otp, err := h.otpService.Validate(loginDto.Code, loginDto.ClientId, loginDto.Phone)

	if err != nil {
		return ctx.Status(err.Status).JSON(err)
	}

	// TODO: create of find User
	user, err := h.userService.LoginWithOtp(otp.Phone)

	if err != nil {
		return ctx.Status(err.Status).JSON(err)
	}

	// TODO: generate Access_token
	token, tErr := h.tokenMaker.CreateToken(user.Phone, config.CommonConfig.AccessTokenDuration)

	if tErr != nil {
		err = errors.NewBadRequest(tErr.Error())
		return ctx.Status(err.Status).JSON(err)
	}

	return ctx.JSON(map[string]string{"access_token": token})
}
