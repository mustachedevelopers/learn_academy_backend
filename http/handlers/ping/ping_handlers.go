package ping

import (
	"github.com/gofiber/fiber/v2"
)

type PingHandler interface {
	GetPing(*fiber.Ctx) error
}

type pingHandler struct{}

func NewHandler() PingHandler {
	return &pingHandler{}
}

func (h *pingHandler) GetPing(ctx *fiber.Ctx) error {
	return ctx.SendString("PONG")
}
