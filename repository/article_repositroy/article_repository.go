package article_repository

import "gitlab.com/MustachDevelopers/learn_academy_backend/domain/article"

type ArticleRepository interface {
	Create(*article.Article) (*article.Article, error)
	FindById(int64) (*article.Article, error)
	Delete(int64) error
	Update(int64, *article.Article) (*article.Article, error)
}

type articleRepository struct{}

func NewRepository() ArticleRepository {
	return &articleRepository{}
}

func (ar articleRepository) Create(*article.Article) (*article.Article, error) {
	return nil, nil
}
func (ar articleRepository) FindById(int64) (*article.Article, error) {
	return nil, nil
}
func (ar articleRepository) Delete(int64) error {
	return nil
}
func (ar articleRepository) Update(int64, *article.Article) (*article.Article, error) {
	return nil, nil
}
