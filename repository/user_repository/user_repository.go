package user_repository

import (
	"fmt"

	"gitlab.com/MustachDevelopers/learn_academy_backend/domain/user"
	"gitlab.com/MustachDevelopers/learn_academy_backend/entity"
	"gitlab.com/MustachDevelopers/learn_academy_backend/utils/crypto_utils"
	"gitlab.com/MustachDevelopers/learn_academy_backend/utils/errors"
	"gorm.io/gorm"
)

type UserRepository interface {
	FindOrCreate(*user.User) (*user.User, *errors.RestErr)
	FindByPhone(string) (*user.User, *errors.RestErr)
	Create(*user.User) (*user.User, *errors.RestErr)
	FindById(int64) (*user.User, *errors.RestErr)
	Find(string) []*user.User
	Update(int64, *user.User) (*user.User, *errors.RestErr)
	Delete(int64) *errors.RestErr
}

type userRepository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) UserRepository {
	return &userRepository{
		db,
	}
}

func (ur userRepository) FindOrCreate(us *user.User) (*user.User, *errors.RestErr) {
	u := &entity.User{
		Phone: us.Phone,
	}

	result := ur.db.Where("phone=?", u.Phone).First(&u)

	if result.Error != nil {

		result = ur.db.Create(&u)
		if result.Error != nil {
			return nil, errors.NewInternalServerError("user can not save or find")
		}
	}

	findUser := &user.User{
		Id:    u.ID,
		Phone: u.Phone,
		Role:  u.Role,
	}
	return findUser, nil
}

func (ur userRepository) FindByPhone(phone string) (*user.User, *errors.RestErr) {
	find_user := &entity.User{
		Phone: phone,
	}

	result := ur.db.Where("phone=?", find_user.Phone).First(&find_user)

	if result.Error != nil {
		return nil, errors.NewNotFound("user not found")
	}

	u := &user.User{
		Id:        find_user.ID,
		Phone:     find_user.Phone,
		Role:      find_user.Role,
		Email:     find_user.Email,
		FirstName: find_user.FirstName,
		LastName:  find_user.LastName,
		Password:  find_user.Password,
	}

	return u, nil
}

func (ur userRepository) Create(us *user.User) (*user.User, *errors.RestErr) {
	var err *errors.RestErr
	us.Password, err = crypto_utils.Hash(us.Password)

	if err != nil {
		return nil, err
	}

	userEntity := entity.User{
		FirstName: us.FirstName,
		LastName:  us.LastName,
		Email:     us.Email,
		Phone:     us.Phone,
		Password:  us.Password,
		Role:      us.Role,
	}

	result := ur.db.Create(&userEntity)

	if result.Error != nil {
		return nil, errors.NewInternalServerError("internal server error")
	}

	us.Id = userEntity.ID
	us.Role = userEntity.Role

	return us, nil
}

func (ur userRepository) FindById(id int64) (*user.User, *errors.RestErr) {
	var us entity.User

	result := ur.db.Where("ID = ?", id).Last(&us)

	if result.Error != nil {
		rerr := errors.NewNotFound(fmt.Sprintf("user with id: %d not found", id))
		return nil, rerr
	}

	u := &user.User{
		Id:        us.ID,
		FirstName: us.FirstName,
		LastName:  us.LastName,
		Email:     us.Email,
		Phone:     us.Phone,
		Password:  us.Password,
		Role:      us.Role,
	}

	return u, nil

}

func (ur userRepository) Find(term string) []*user.User {
	var users []entity.User
	us := make([]*user.User, 0)
	term = "%" + term + "%"
	result := ur.db.Where("first_name LIKE ?", term).Or("last_name LIKE ?", term).Or("email LIKE ?", term).Find(&users)

	if result.Error != nil {
		return us
	}

	for _, findUser := range users {
		u := &user.User{
			Id:        findUser.ID,
			FirstName: findUser.FirstName,
			LastName:  findUser.LastName,
			Email:     findUser.Email,
			Phone:     findUser.Phone,
			Password:  findUser.Password,
			Role:      findUser.Role,
		}

		us = append(us, u)
	}

	return us
}

func (ur userRepository) Update(id int64, us *user.User) (*user.User, *errors.RestErr) {
	u := &entity.User{
		ID: id,
	}
	result := ur.db.Last(&u)

	if result.Error != nil {
		return nil, errors.NewNotFound("user not found")
	}

	if us.FirstName != "" {
		u.FirstName = us.FirstName
	}

	if us.LastName != "" {
		u.LastName = us.LastName
	}

	if us.Email != "" {
		u.Email = us.Email
	}

	if us.Role != "" {
		u.Role = us.Role
	}

	if us.Phone != "" {
		u.Phone = us.Phone
	}

	if us.Password != "" {
		us.Password, _ = crypto_utils.Hash(us.Password)
	}

	result = ur.db.Save(u)

	if result.Error != nil {
		return nil, errors.NewBadRequest(result.Error.Error())
	}

	return us, nil
}

func (ur userRepository) Delete(id int64) *errors.RestErr {
	u := &entity.User{
		ID: id,
	}

	result := ur.db.Delete(u)

	if result.Error != nil {
		return errors.NewBadRequest(result.Error.Error())
	}

	return nil
}
