package otp_repository

import (
	"gitlab.com/MustachDevelopers/learn_academy_backend/domain/otp"
	"gitlab.com/MustachDevelopers/learn_academy_backend/entity"
	"gitlab.com/MustachDevelopers/learn_academy_backend/utils/errors"
	"gorm.io/gorm"
)

type OtpRepository interface {
	Save(*otp.Otp) *errors.RestErr
	FindOne(int, string) (*otp.Otp, *errors.RestErr)
}

type otpRepository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) OtpRepository {
	return &otpRepository{
		db,
	}
}

func (repo otpRepository) Save(o *otp.Otp) *errors.RestErr {
	new_otp := &entity.Otp{
		Code:     o.Code,
		Expired:  o.Expired,
		Phone:    o.Phone,
		ClientId: o.ClientId,
		Attemp:   o.Attemp,
	}

	result := repo.db.Create(&new_otp)

	if result.Error != nil {
		return errors.NewInternalServerError("internal server error")
	}

	return nil
}

func (repo otpRepository) FindOne(code int, phone string) (*otp.Otp, *errors.RestErr) {
	var o *entity.Otp

	result := repo.db.Where("code = ? AND phone = ? AND attemp > ?", code, phone, 0).Last(&o)

	if result.Error != nil {
		return nil, errors.NewNotFound("invalid otp send")
	}

	if o == nil {
		return nil, errors.NewBadRequest("otp does not valid")
	}

	new_otp := &otp.Otp{
		Code:     o.Code,
		Expired:  o.Expired,
		Phone:    o.Phone,
		Attemp:   o.Attemp,
		ClientId: o.ClientId,
	}

	return new_otp, nil
}
