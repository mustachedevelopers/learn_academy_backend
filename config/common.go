package config

import (
	"os"
	"time"
)

type common struct {
	Port      string
	SMSApiKey string
	Symmetric string
	AccessTokenDuration time.Duration
}

var (
	CommonConfig = &common{}
)

func setCommonConfig() {
	CommonConfig.Port = os.Getenv("PORT")
	CommonConfig.SMSApiKey = os.Getenv("SMS_API_KEY")
	CommonConfig.Symmetric = os.Getenv("SYMMETRIC_KEY")
	CommonConfig.AccessTokenDuration, _ = time.ParseDuration(os.Getenv("ACCESS_TOKEN_DURATION"))
}
