package config

import "os"

type database struct {
	Host     string
	Port     string
	Username string
	Password string
	DBName   string
	SSLMode  string
	TimeZone string
}

var (
	DatabaseConfig = &database{}
)

func setDatabaseConfig() {
	DatabaseConfig.Host = os.Getenv("DB_HOST")
	DatabaseConfig.Port = os.Getenv("DB_PORT")
	DatabaseConfig.Username = os.Getenv("DB_USER")
	DatabaseConfig.Password = os.Getenv("DB_PASS")
	DatabaseConfig.DBName = os.Getenv("DB_NAME")
	DatabaseConfig.SSLMode = os.Getenv("DB_SSLMODE")
	DatabaseConfig.TimeZone = os.Getenv("DB_TIMEZONE")
}
