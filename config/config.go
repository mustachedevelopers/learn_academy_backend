package config

import (
	"github.com/joho/godotenv"
)

func ParseEnv(environment string) {
	var fn string
	
	if environment == "prod" {
		fn = ".env"
	} else {
		fn = "development.env"
	}

	err := godotenv.Load(fn)

	if err != nil {
		panic(err)
	}

	setConfig()
}


func setConfig() {
	setCommonConfig()
	setDatabaseConfig()
	setRedisConfig()
}
