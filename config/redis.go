package config

import "os"

type redis struct {
	Host     string
	Port     string
	Password string
}

var RedisConfig = &redis{}

func setRedisConfig() {
	RedisConfig.Host = os.Getenv("REDIS_HOST")
	RedisConfig.Port = os.Getenv("REDIS_PORT")
	RedisConfig.Password = os.Getenv("REDIS_PASSWORD")
}
