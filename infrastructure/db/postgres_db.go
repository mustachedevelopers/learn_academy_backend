package db

import (
	"fmt"

	"gitlab.com/MustachDevelopers/learn_academy_backend/config"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"gitlab.com/MustachDevelopers/learn_academy_backend/entity"
)

var (
	Client *gorm.DB
)

func SetDB() {

	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=%s TimeZone=%s",
		config.DatabaseConfig.Host,
		config.DatabaseConfig.Username,
		config.DatabaseConfig.Password,
		config.DatabaseConfig.DBName,
		config.DatabaseConfig.Port,
		config.DatabaseConfig.SSLMode,
		config.DatabaseConfig.TimeZone,
	)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("Can not connect to database.")
	}

	db.AutoMigrate(&entity.Otp{}, &entity.User{})

	Client = db
}
