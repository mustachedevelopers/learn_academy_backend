package sms

import (
	"github.com/kavenegar/kavenegar-go"
	"gitlab.com/MustachDevelopers/learn_academy_backend/config"
)

func ConnectSMS() *kavenegar.Kavenegar {
	return kavenegar.New(config.CommonConfig.SMSApiKey)
}
