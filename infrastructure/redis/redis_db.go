package redis

import (
	"github.com/go-redis/redis/v8"
	"gitlab.com/MustachDevelopers/learn_academy_backend/config"
)

var Client *redis.Client

func ConnectRedis() {
	Client = redis.NewClient(&redis.Options{
		Addr:     config.RedisConfig.Host + ":" + config.RedisConfig.Port,
		Password: config.RedisConfig.Password,
		DB:       0,
	})
}
